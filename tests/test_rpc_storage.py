import unittest
from datetime import datetime
from typing import Callable, Any, TypeVar, Tuple

import grpc
import grpc_testing
from google.protobuf.descriptor import MethodDescriptor, ServiceDescriptor
from google.protobuf.empty_pb2 import Empty
from google.protobuf.timestamp_pb2 import Timestamp
from google.protobuf.wrappers_pb2 import BoolValue
from grpc import StatusCode, Channel
from grpc.framework.foundation import logging_pool
from optuna import structs
from optuna.distributions import IntUniformDistribution
from optuna.storages import BaseStorage
from optuna.structs import StudySummary

from optuna_firestore_proto import study_pb2, trial_pb2
from optuna_firestore_proto.study_pb2 import CreateNewStudyIdRequest, SetStudyUserAttrRequest, SetStudyDirectionRequest, \
    SetStudySystemAttrRequest, MAXIMIZE, GetAllStudySummariesResponse, StudySummaryMsg, MINIMIZE
from optuna_firestore_proto.study_pb2_grpc import StudyServiceStub
from optuna_firestore_proto.trial_pb2 import CreateNewTrialIdResponse, CreateNewTrialIdRequest, SetTrialStateRequest, \
    SetTrialParamRequest, SetTrialValueRequest, SetTrialIntermediateValueRequest, SetTrialUserAttrRequest, RUNNING, \
    TrialParamMsg, GetAllTrialsResponse, GetAllTrialsRequest, SetTrialSystemAttrRequest
from optuna_firestore_proto.trial_pb2_grpc import TrialServiceStub
from optuna_firestore_storage.rpc_storage import RPCStorage


def create_rpc_storage(channel: Channel):
    study_service_stub = StudyServiceStub(channel)
    trial_service_stub = TrialServiceStub(channel)
    return RPCStorage(study_service_stub, trial_service_stub)


T = TypeVar('T')


class RPCStorageTest(unittest.TestCase):

    def setUp(self):
        self.client_execution_thread_pool = logging_pool.pool(1)
        self.real_time_channel = grpc_testing.channel(None, grpc_testing.strict_real_time())
        self.study_service: ServiceDescriptor = study_pb2.DESCRIPTOR.services_by_name['StudyService']
        self.trial_service: ServiceDescriptor = trial_pb2.DESCRIPTOR.services_by_name['TrialService']

    def tearDown(self):
        self.client_execution_thread_pool.shutdown(wait=True)

    def test_create_new_study_id(self):
        def test_fn(storage: BaseStorage):
            return storage.create_new_study_id('my new study')

        rpc_method = self.study_service.methods_by_name['CreateNewStudyId']
        msg = study_pb2.StudyMsg(study_id=1, study_name='', user_attributes={})

        result, req = self._run_rpc(test_fn, rpc_method, msg)

        self.assertEqual(CreateNewStudyIdRequest(study_name='my new study'), req)
        self.assertEqual(1, result)

    def test_create_new_study_id_with_already_existing(self):
        def test_fn(storage: BaseStorage):
            return storage.create_new_study_id('my new study')

        rpc_method = self.study_service.methods_by_name['CreateNewStudyId']
        msg = study_pb2.StudyMsg(study_id=1, study_name='', user_attributes={})

        def run():
            self._run_rpc(test_fn, rpc_method, msg, StatusCode.ALREADY_EXISTS)

        self.assertRaises(structs.DuplicatedStudyError, run)

    def test_set_study_user_attr(self):
        def test_fn(storage: BaseStorage):
            return storage.set_study_user_attr(1, 'my key', {'my attr': 123})

        rpc_method = self.study_service.methods_by_name['SetStudyUserAttr']
        msg = Empty()

        _, req = self._run_rpc(test_fn, rpc_method, msg)

        expected = SetStudyUserAttrRequest(
            study_id=1,
            key='my key',
            value_json='{\"my attr\":123}'
        )
        self.assertEqual(expected, req)

    def test_set_study_direction(self):
        def test_fn(storage: BaseStorage):
            return storage.set_study_direction(1, structs.StudyDirection.MAXIMIZE)

        rpc_method = self.study_service.methods_by_name['SetStudyDirection']
        msg = Empty()

        result, req = self._run_rpc(test_fn, rpc_method, msg)

        expected = SetStudyDirectionRequest(study_id=1, direction=structs.StudyDirection.MAXIMIZE.value)
        self.assertEqual(expected, req)

    def test_set_study_direction_with_updating(self):
        def test_fn(storage: BaseStorage):
            return storage.set_study_direction(1, structs.StudyDirection.MAXIMIZE)

        rpc_method = self.study_service.methods_by_name['SetStudyDirection']
        msg = Empty()

        def run():
            self._run_rpc(test_fn, rpc_method, msg, grpc.StatusCode.FAILED_PRECONDITION)

        self.assertRaises(ValueError, run)

    def test_set_study_system_attr(self):
        def test_fn(storage: BaseStorage):
            return storage.set_study_system_attr(1, 'my key', {'my attr': 123})

        rpc_method = self.study_service.methods_by_name['SetStudySystemAttr']
        msg = Empty()

        _, req = self._run_rpc(test_fn, rpc_method, msg)

        expected = SetStudySystemAttrRequest(
            study_id=1,
            key='my key',
            value_json='{\"my attr\":123}'
        )
        self.assertEqual(expected, req)

    def test_get_study_id_from_name(self):
        def test_fn(storage: BaseStorage):
            return storage.get_study_id_from_name("my study")

        rpc_method = self.study_service.methods_by_name['GetStudyFromName']
        msg = study_pb2.StudyMsg(study_id=4, study_name='my study', user_attributes={})

        result, _ = self._run_rpc(test_fn, rpc_method, msg)

        self.assertEqual(4, result)

    def test_get_study_name_from_id(self):
        def test_fn(storage: BaseStorage):
            return storage.get_study_name_from_id(1)

        rpc_method = self.study_service.methods_by_name['GetStudyFromId']
        msg = study_pb2.StudyMsg(study_id=1, study_name='my study', user_attributes={})

        result, _ = self._run_rpc(test_fn, rpc_method, msg)

        self.assertEqual('my study', result)

    def test_get_study_direction(self):
        def test_fn(storage: BaseStorage):
            return storage.get_study_direction(1)

        rpc_method = self.study_service.methods_by_name['GetStudyFromId']
        msg = study_pb2.StudyMsg(study_id=1, study_name='my study', direction=MAXIMIZE)

        result, _ = self._run_rpc(test_fn, rpc_method, msg)

        self.assertEqual(structs.StudyDirection.MAXIMIZE, result)

    def test_get_study_user_attrs(self):
        def test_fn(storage: BaseStorage):
            return storage.get_study_user_attrs(1)

        rpc_method = self.study_service.methods_by_name['GetStudyFromId']
        msg = study_pb2.StudyMsg(study_id=1, study_name='my study', user_attributes={
            'my attr1': '1',
            'my attr2': '"1"',
            'my attr3': '{\"3 attr\":123}',
        })

        result, _ = self._run_rpc(test_fn, rpc_method, msg)

        expected = {
            'my attr1': 1,
            'my attr2': '1',
            'my attr3': {
                '3 attr': 123
            },
        }
        self.assertEqual(expected, result)

    def test_get_study_system_attrs(self):
        def test_fn(storage: BaseStorage):
            return storage.get_study_system_attrs(1)

        rpc_method = self.study_service.methods_by_name['GetStudyFromId']
        msg = study_pb2.StudyMsg(study_id=1, study_name='my study', system_attributes={
            'my attr1': '1',
            'my attr2': '"1"',
            'my attr3': '{\"3 attr\":123}',
        })

        result, _ = self._run_rpc(test_fn, rpc_method, msg)

        expected = {
            'my attr1': 1,
            'my attr2': '1',
            'my attr3': {
                '3 attr': 123
            },
        }
        self.assertEqual(expected, result)

    def test_get_all_study_summaries(self):
        def test_fn(storage: BaseStorage):
            return storage.get_all_study_summaries()

        rpc_method = self.study_service.methods_by_name['GetAllStudySummaries']

        best_trial_msg, trial = self._make_trial_msg_and_trial()
        datetime_start = Timestamp()
        datetime_start.FromDatetime(dt=datetime(2019, 2, 10))

        msg = GetAllStudySummariesResponse(study_summaries=[
            StudySummaryMsg(
                study_id=1,
                study_name='my study',
                direction=MINIMIZE,
                best_trial=best_trial_msg,
                user_attributes={
                    'user': '1'
                },
                system_attributes={
                    'sys': '2'
                },
                n_trials=3,
                datetime_start=datetime_start
            )
        ])

        result, req = self._run_rpc(test_fn, rpc_method, msg)

        expected = [
            StudySummary(
                study_id=1,
                study_name='my study',
                direction=structs.StudyDirection.MINIMIZE,
                best_trial=trial,
                user_attrs={
                    'user': 1
                },
                system_attrs={
                    'sys': 2
                },
                n_trials=3,
                datetime_start=datetime(2019, 2, 10)
            )
        ]
        self.assertEqual(expected, result)

    def test_create_new_trial_id(self):
        def test_fn(storage: BaseStorage):
            return storage.create_new_trial_id(study_id=1)

        rpc_method = self.trial_service.methods_by_name['CreateNewTrialId']
        msg = CreateNewTrialIdResponse(trial_id=2)

        result, req = self._run_rpc(test_fn, rpc_method, msg)

        self.assertEqual(CreateNewTrialIdRequest(study_id=1), req)
        self.assertEqual(2, result)

    def test_set_trial_state(self):
        def test_fn(storage: BaseStorage):
            return storage.set_trial_state(trial_id=1, state=structs.TrialState.COMPLETE)

        rpc_method = self.trial_service.methods_by_name['SetTrialState']
        msg = Empty()

        _, req = self._run_rpc(test_fn, rpc_method, msg)

        expected = SetTrialStateRequest(trial_id=1, state=structs.TrialState.COMPLETE.value)
        self.assertEqual(expected, req)

    def test_set_trial_param(self):
        def test_fn(storage: BaseStorage):
            return storage.set_trial_param(1, "my param", 0.1, IntUniformDistribution(-10, 10))

        rpc_method = self.trial_service.methods_by_name['SetTrialParam']
        msg = BoolValue(value=False)

        result, req = self._run_rpc(test_fn, rpc_method, msg)

        expected_request = SetTrialParamRequest(
            trial_id=1,
            param_name='my param',
            param_value=0.1,
            distribution_json="{\"name\": \"IntUniformDistribution\", \"attributes\": {\"low\": -10, \"high\": 10}}"
        )
        self.assertEqual(expected_request, req)
        self.assertEqual(False, result)

    def test_get_trial_param(self):
        def test_fn(storage: BaseStorage):
            return storage.get_trial_param(1, "my param")

        rpc_method = self.trial_service.methods_by_name['GetTrialFromId']

        trial_param_msg = trial_pb2.TrialParamMsg(param_value=0.1, is_exist=True)
        trial_msg = trial_pb2.TrialMsg(trial_params={'my param': trial_param_msg})

        result, _ = self._run_rpc(test_fn, rpc_method, trial_msg)

        self.assertEqual(0.1, result)

    def test_set_trial_value(self):
        def test_fn(storage: BaseStorage):
            return storage.set_trial_value(1, 0.1)

        rpc_method = self.trial_service.methods_by_name['SetTrialValue']
        msg = Empty()

        result, req = self._run_rpc(test_fn, rpc_method, msg)

        self.assertEqual(SetTrialValueRequest(trial_id=1, value=0.1), req)

    def test_set_trial_intermediate_value(self):
        def test_fn(storage: BaseStorage):
            return storage.set_trial_intermediate_value(1, 2, 0.1)

        rpc_method = self.trial_service.methods_by_name['SetTrialIntermediateValue']
        msg = BoolValue(value=True)

        result, req = self._run_rpc(test_fn, rpc_method, msg)

        expected_request = SetTrialIntermediateValueRequest(
            trial_id=1,
            step=2,
            intermediate_value=0.1
        )
        self.assertEqual(expected_request, req)
        self.assertEqual(True, result)

    def test_set_trial_user_attr(self):
        def test_fn(storage: BaseStorage):
            return storage.set_trial_user_attr(1, 'my key', {'my attr': 123})

        rpc_method = self.trial_service.methods_by_name['SetTrialUserAttr']
        msg = Empty()

        _, req = self._run_rpc(test_fn, rpc_method, msg)

        expected = SetTrialUserAttrRequest(
            trial_id=1,
            key='my key',
            value_json='{\"my attr\":123}'
        )
        self.assertEqual(expected, req)

    def test_set_trial_system_attr(self):
        def test_fn(storage: BaseStorage):
            return storage.set_trial_system_attr(1, 'my key', {'my attr': 123})

        rpc_method = self.trial_service.methods_by_name['SetTrialSystemAttr']
        msg = Empty()

        _, req = self._run_rpc(test_fn, rpc_method, msg)

        expected = SetTrialSystemAttrRequest(
            trial_id=1,
            key='my key',
            value_json='{\"my attr\":123}'
        )
        self.assertEqual(expected, req)

    def test_get_trial(self):
        def test_fn(storage: BaseStorage):
            return storage.get_trial(1)

        rpc_method = self.trial_service.methods_by_name['GetTrialFromId']
        trial_msg, expected_trial = self._make_trial_msg_and_trial()

        result, _ = self._run_rpc(test_fn, rpc_method, trial_msg)

        self.assertEqual(expected_trial, result)

    def test_get_all_trials(self):
        def test_fn(storage: BaseStorage):
            return storage.get_all_trials(study_id=1)

        rpc_method = self.trial_service.methods_by_name['GetAllTrials']

        trial_msg, expected_trial = self._make_trial_msg_and_trial()
        res = GetAllTrialsResponse(trials=[trial_msg])

        result, req = self._run_rpc(test_fn, rpc_method, res)

        self.assertEqual(GetAllTrialsRequest(study_id=1), req)
        self.assertEqual([expected_trial], result)

    def test_get_n_trials_with_state(self):
        def test_fn(storage: BaseStorage):
            return storage.get_n_trials(study_id=1, state=structs.TrialState.COMPLETE)

        rpc_method = self.trial_service.methods_by_name['GetAllTrials']

        trial_msg0, _ = self._make_trial_msg_and_trial()
        trial_msg1, _ = self._make_trial_msg_and_trial()
        trial_msg2, _ = self._make_trial_msg_and_trial()

        trial_msg0.state = structs.TrialState.RUNNING.value
        trial_msg1.state = structs.TrialState.COMPLETE.value
        trial_msg2.state = structs.TrialState.COMPLETE.value

        res = GetAllTrialsResponse(trials=[trial_msg0, trial_msg1, trial_msg2])

        result, req = self._run_rpc(test_fn, rpc_method, res)

        self.assertEqual(2, result)

    def test_get_n_trials_without_state(self):
        def test_fn(storage: BaseStorage):
            return storage.get_n_trials(study_id=1)

        rpc_method = self.trial_service.methods_by_name['GetAllTrials']

        trial_msg0, _ = self._make_trial_msg_and_trial()
        trial_msg1, _ = self._make_trial_msg_and_trial()
        trial_msg2, _ = self._make_trial_msg_and_trial()

        trial_msg0.state = structs.TrialState.RUNNING.value
        trial_msg1.state = structs.TrialState.COMPLETE.value
        trial_msg2.state = structs.TrialState.COMPLETE.value

        res = GetAllTrialsResponse(trials=[trial_msg0, trial_msg1, trial_msg2])

        result, req = self._run_rpc(test_fn, rpc_method, res)

        self.assertEqual(3, result)

    def _run_rpc(self, test_fn: Callable[[BaseStorage], T],
                 method_descriptor: MethodDescriptor,
                 response: Any,
                 status_code: StatusCode = StatusCode.OK) -> Tuple[T, Any]:
        def run(_, channel):
            storage = create_rpc_storage(channel)
            return test_fn(storage)

        future = self.client_execution_thread_pool.submit(run, None, self.real_time_channel)
        _, req, rpc = self.real_time_channel.take_unary_unary(method_descriptor)
        rpc.terminate(response, (), status_code, '')

        return future.result(), req

    @staticmethod
    def _make_trial_msg_and_trial() -> Tuple[trial_pb2.TrialMsg, structs.FrozenTrial]:
        datetime_start = Timestamp()
        datetime_start.FromDatetime(dt=datetime(2019, 2, 18))

        msg = trial_pb2.TrialMsg(
            trial_id=1,
            study_id=2,
            state=RUNNING,
            datetime_start=datetime_start,
            # datetime_complete=datetime_complete,
            value=0.1,
            trial_params={
                'param1': TrialParamMsg(
                    param_value=-9,
                    distribution_json="{\"name\": \"IntUniformDistribution\", \"attributes\": {\"low\": -10, \"high\": 10}}")
            },
            trial_values={
                0: 0.11
            },
            user_attributes={
                'my key': '{\"my attr\":123}'
            }
        )

        trial = structs.FrozenTrial(
            trial_id=1,
            state=structs.TrialState.RUNNING,
            value=0.1,
            datetime_start=datetime(2019, 2, 18),
            datetime_complete=None,
            params={
                'param1': -9,
            },
            user_attrs={
                'my key': {
                    'my attr': 123
                }
            },
            system_attrs={},
            intermediate_values={
                0: 0.11
            },
            params_in_internal_repr={
                'param1': -9.0,
            },
        )

        return msg, trial
