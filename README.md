# Optuna Firestore Storage

[![PyPI version](https://badge.fury.io/py/optuna-firestore-storage.svg)](https://badge.fury.io/py/optuna-firestore-storage)
[![pipeline status](https://gitlab.com/optuna-firestore-adapter/optuna-firestore-storage/badges/master/pipeline.svg)](https://gitlab.com/optuna-firestore-adapter/optuna-firestore-storage/commits/master)

Optuna Firestore Storage is the Firestore adapter of Optuna. By using this adapter, Optuna user can use Firestore as a backend DB.

## Benefits

* As it observes the realtime updates from Firestore, there is almost no overhead caused by network latency.
* Firestore has some more advantages.
  * Fully managed. No need to even launch or stop.
  * Cost effective than RDB.
  * Easy to setup.
  * Scalable.
  
## Getting started

It requires a proxy, which observes the realtime updates from Firestore and manages trials in memory DB. [Download](https://gitlab.com/optuna-firestore-adapter/optuna-firestore-proxy/releases) and run it.

```
tar xvzf optuna-firestore-proxy.tar.gz
./optuna-firestore-proxy/bin/optuna-firesore-proxy
```

If you use a service account credential to connect to Firestore, set GOOGLE_APPLICATION_CREDENTIALS variable.

```
GOOGLE_APPLICATION_CREDENTIALS=/path/to/credential.json ./optuna-firestore-proxy/bin/optuna-firesore-proxy
```

Next, install python adapter from pip.

```
pip install optuna-firestore-storage
```

Then, your code will be something like below.

```python
import optuna
from optuna_firestore_storage.rpc_storage import create_rpc_storage

storage = create_rpc_storage('localhost:50051')
study = optuna.create_study(study_name='my study', storage=storage)
```
  
 


