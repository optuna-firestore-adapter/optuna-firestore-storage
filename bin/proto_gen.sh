#!/usr/bin/env bash

PROTO_SRC=./proto
PROTO_DST=./

mkdir -p ${PROTO_DST}

python \
    -m grpc_tools.protoc \
    -I ${PROTO_SRC} \
    --python_out=${PROTO_DST} \
    --grpc_python_out=${PROTO_DST} \
    ./proto/optuna_firestore_proto/*.proto

protoc \
    -I ${PROTO_SRC} \
    --plugin=$(which protoc-gen-mypy) \
    --python_out=${PROTO_DST} \
    --mypy_out=${PROTO_DST} \
    ./proto/optuna_firestore_proto/*.proto

